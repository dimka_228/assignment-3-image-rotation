#ifndef SIMPLE_MATH_H
#define SIMPLE_MATH_H

#define PI 3.1415926535897932384650288

double sin_grad(double x);
double cos_grad(double x);

#endif
