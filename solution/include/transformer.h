#ifndef TRANSFORMER_H
#define TRANSFORMER_H

struct coord{size_t x,y; };
struct image rotate(struct image const source);
struct image rotate_by_angle(struct image const source, double angle);

#endif
