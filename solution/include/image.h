#ifndef IMAGE_H
#define IMAGE_H

#include "printer.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct image {
	size_t width, height;
	struct pixel *data;
};

struct pixel {
	uint8_t r, g, b;
};

struct image image_init(uint32_t width, uint32_t height);

void image_free(struct image *img);


bool image_set_pixel(struct image *img, struct pixel const p, size_t x, size_t y);

struct pixel image_get_pixel(struct image const* img, size_t x, size_t y);

size_t image_get_size_bytes(struct image const* img);


#endif
