#ifndef BMP_H
#define BMP_H

#include "headers.h"
#include "image.h"
#include <stdbool.h>
#include <stdio.h>

enum read_status{OK, READ_ERROR, INVALID_HEADER};

enum read_status from_bmp(FILE* file, struct image* img);
bool to_bmp(FILE* file, struct image const* img);


#endif

