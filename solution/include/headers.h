#ifndef HEADERS_H
#define HEADERS_H

#include "image.h"
#include <stdbool.h>
#include <stdlib.h>

#define BF_TYPE 0x4D42
#define B_OFF_BITS 54
#define B_SIZE 40
#define B_PLANES 1
#define BIT_COUNT 24
#define B_X_PELS_PER_METER 2834
#define B_Y_PELS_PER_METER 2834

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct bmp_header create_headers(const struct image * img);

bool validate_header(struct bmp_header const * header);

bool read_header(struct bmp_header* header, FILE* file);

bool write_header(FILE* file, struct bmp_header* header);
#endif


