#include "headers.h"
#include "image.h"
#include "padding.h"
#include <stdbool.h>
#include <stdlib.h>


struct bmp_header create_headers(const struct image * img){
	struct bmp_header header = {0};
	header.bfType = BF_TYPE;
        header.bfileSize = sizeof(struct bmp_header) + image_get_size_bytes(img) + padding_size_bytes(img);
        header.bOffBits = B_OFF_BITS;
        header.biSize = B_SIZE;
        header.biWidth = img->width;
        header.biHeight = img->height;
        header.biPlanes = B_PLANES;
        header.biBitCount = BIT_COUNT;
        header.biSizeImage = header.bfileSize - header.bOffBits;
        header.biXPelsPerMeter = B_X_PELS_PER_METER;
        header.biYPelsPerMeter = B_Y_PELS_PER_METER;
        return header;
}

bool validate_header(struct bmp_header const * header){
	return header->biHeight > 0 && header->biWidth > 0 && header->bfType == BF_TYPE;
}

bool read_header(struct bmp_header* header, FILE* file){
        return fread(header, sizeof(struct bmp_header), 1, file);
}

bool write_header(FILE* file, struct bmp_header* header){
        return fwrite(header, sizeof (struct bmp_header), 1, file) == 1;
}

