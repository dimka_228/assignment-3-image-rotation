#include "file.h"
#include <stdbool.h>
#include <stdio.h>
bool open_file ( FILE** file, const char* path, const char* mode){
    if (!path) return false;
    *file = fopen(path, mode);
    if (!*file) return false;
    return true;
}

bool close_file (FILE** file){
    if (!*file) return false;
    if (fclose(*file)) return false;
    return true;
}

