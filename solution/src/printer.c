#include "printer.h"
#include "stdio.h"

void print_error(char * error){
	fprintf(stderr, "ERROR: %s \n", error);
}

void print(char * str){
	fprintf(stdout, "%s \n", str);
}
