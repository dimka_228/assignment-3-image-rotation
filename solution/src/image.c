#include "image.h"
#include <stdbool.h>
#include <stdlib.h>

struct image image_init(uint32_t width, uint32_t height){
	struct image img = {0};
    img.width = width;
    img.height = height;
    img.data = malloc (width * height * sizeof (struct pixel));
    return img;
}

void image_free(struct image *img){
	free(img->data);
}

bool image_set_pixel(struct image *img, struct pixel const pixel, size_t x, size_t y) {
    if (x < img->width && y < img->height) {
        img->data[y * img->width + x] = pixel;
        return true;
    }
    return false;
}

size_t image_get_size_bytes(struct image const* img) {
    return img->width * img->height * sizeof(struct pixel);
}

struct pixel image_get_pixel(struct image const* img, size_t x, size_t y) {
    return img->data[y * img->width + x];
}





