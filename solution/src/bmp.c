#include "bmp.h"
#include "headers.h"
#include "image.h"
#include "padding.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static bool read_pixel(FILE *file, struct pixel *pix){
    return fread(pix, sizeof(struct pixel), 1, file) == 1;
}

static bool write_pixel(FILE *file, struct pixel *pix){
    return fwrite(pix, sizeof(struct pixel), 1, file) == 1;
}

static bool read_pixels(FILE *file, struct bmp_header const *headers, struct image *img){
    struct pixel pix;
    img->width = headers->biWidth;
    img->height = headers->biHeight;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));
    if (!img->data)
        return false;
    for (size_t i = 0; i < img->height; i++)
    {
        for (size_t j = 0; j < img->width; j++)
        {
            if (!read_pixel(file, &pix))
            {
                image_free(img);
                return false;
            }
            image_set_pixel(img, pix, j, i);
        }
        fseek(file, calculate_padding(img->width), SEEK_CUR);
    }
    return true;
}

static bool write_pixels(FILE *file, struct image const *img){
    struct pixel pix;
    for (size_t i = 0; i < img->height; i++)
    {
        for (size_t j = 0; j < img->width; j++)
        {
            pix = image_get_pixel(img, j, i);
            if (!write_pixel(file, &pix))
                return false;
        }
        if (!write_padding(file, img->width))
            return false;
    }
    return true;
}

enum read_status from_bmp(FILE *file, struct image *img){
    if (!file || !img)
        return READ_ERROR;
    struct bmp_header header = {0};
    if (!read_header(&header, file))
        return READ_ERROR;
    if (!validate_header(&header))
        return INVALID_HEADER;
    if (!read_pixels(file, &header, img))
        return READ_ERROR;
    return OK;
}

bool to_bmp(FILE *file, struct image const *img){
    struct bmp_header header;
    if (!file || !img)
        return false;
    header = create_headers(img);
    if (!write_header(file, &header))
        return false;
    return write_pixels(file, img);
}
