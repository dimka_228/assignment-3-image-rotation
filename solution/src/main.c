#include <stdio.h>
#include <sysexits.h>

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "transformer.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
    	print_error("Wrong number of arguments");
        return EX_USAGE;
    }
    FILE* in = NULL;
    if (!open_file(&in, argv[1], "rb")){
        print_error("Unable to open file");
        return EX_NOINPUT;
    }
    struct image img = {0};
    switch (from_bmp(in, &img)) {
        case INVALID_HEADER:
            print_error("Invalid header");
            close_file(&in);
            return EX_DATAERR;
        case READ_ERROR:
            print_error("Unable to open file");
            close_file(&in);
            return EX_NOINPUT;
        case OK:
            break;
    }
    if(!close_file(&in)) {
        print_error("Unable to close file");
        image_free(&img);
        return EX_IOERR;
    }
    struct image new_img = rotate(img);
    image_free(&img);
    FILE* out = NULL;
    if(!open_file(&out, argv[2], "wb")) {
        print_error("Unable to open or create file");
        image_free(&new_img);
        return EX_CANTCREAT;
    }
    if (!to_bmp(out, &new_img)) {
        print_error("Unable to write in output file");
        image_free(&new_img);
        close_file(&out);
        return EX_IOERR;
    }
    image_free(&new_img);
    if (!close_file(&out)) {
        print_error("Unable to close file");
        return EX_IOERR;
    }
	print("Image rotated");
    return EX_OK;
}
