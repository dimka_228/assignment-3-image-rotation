#include "image.h"
#include "simple_math.h"
#include "transformer.h"
#include <stdint.h>
struct image rotate(struct image const source) {
	struct image rotated = image_init(source.height, source.width);
	for(size_t i = 0; i < source.height; i++){
		for(size_t j = 0; j < source.width; j++) {
			image_set_pixel(&rotated, image_get_pixel(&source, j, source.height - i - 1), i, j);
		}
	}
	return rotated;
}


static struct coord rotate_pixel(double theta, struct coord center, struct coord pos)
{
    double x, y;
    x = (double)(pos.x-center.x);
    y = (double)(pos.y-center.y);

    return (struct coord){
		(size_t)(x* cos_grad(theta) - y*sin_grad(theta)) + center.x, // x
    	(size_t)(x* sin_grad(theta) + y*cos_grad(theta)) + center.y // y
	};
}

//Дополнительное задание. поворот на произвольный угол. в градусах
struct image rotate_by_angle(struct image const source, double angle){
	struct coord center = {(size_t) source.width/2, (size_t) source.height/2};
	uint32_t max = source.width+source.height;//(uint32_t) sqrt(source.width*source.width + source.height*source.height+100);
	struct image rotated = image_init(max,max);
	struct coord rotated_center = {(size_t) rotated.width/2, (size_t) rotated.height/2};

	size_t dx = rotated_center.x-center.x;
	size_t dy = rotated_center.y-center.y;
	for(size_t i = 0; i < source.height; i++){
		for(size_t j = 0; j < source.width; j++) {
			struct coord coords = {j,i};
			struct coord rotated_coords = rotate_pixel(angle, center, coords);

			image_set_pixel(&rotated, image_get_pixel(&source, coords.x, coords.y), rotated_coords.x+dx, rotated_coords.y+dy);
		}	
	}
	return rotated;
}



